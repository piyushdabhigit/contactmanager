/**
 * @description HTTP server module
 * @param http
 */
import https from "https";
import http from "http";
import fs from  "fs";
import path from  "path";


/**
 * @description Express Framework module
 * @param express
 */
import express from "express";


/**
 * @description Configure env variables
 * @param config
 */
import dotenv from "dotenv-safe";
dotenv.config();
/**
 * @description Database config class
 * @param DBConfig
 */
import DBConfig from "./config/db.conf.js";


/**
 * @description Routes config class
 * @param Routes
 */

import { initRoutesV1 } from "./V1/config/routes.conf.js";

import ApplicationConfig from "./config/app.conf.js";

/**
 * @description Swagger UI npm module
 * @param Routes
 */
import swaggerUi from "swagger-ui-express";

/**
 * @description Swagger Documnet
 * @param Routes
 */
import { createRequire } from "module";
const require = createRequire(import.meta.url);


 

/**
 * @description Create application with Express Framework
 * @param app
 */
const app = express();
 
app.use(express.static('public'));  
app.use('/images', express.static('images')); 
var options = {}
 
 DBConfig.init();


/**
 * @description Configure Application
 */
ApplicationConfig.init(app);

/**
 * @description Configure Routes
 */

initRoutesV1(app);
 
const server = http.createServer(
  {
   
}, app);



server.listen(process.env.PORT, process.env.IP, () => {
   
    console.log('HTTP Server running on port ' + 3000);
});

/**
 * @description Application object
 * @module app
 */
// module.exports = app;
export default app;
