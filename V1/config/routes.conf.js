

import usersRouter from "../api/users/routes/users.js";
import contactRouter from "../api/contact/routes/contact.js";
  

/**
 * Init routes config
 * @param app
 */
export function initRoutesV1(app) {
  const startTime = new Date();
  let version="1"
  // Insert routes below
 
   app.use("/"+version+"/users", usersRouter); 
   app.use("/"+version+"/contact", contactRouter); 
  }
