/**
 * @description Express Framework module
 * @param express
 */
import express from "express";

/**
 * @description User Controller
 * @param ContactController
 */

import ContactController from "../controller/ContactController.js";
import AuthMiddleware from "../../../middlewares/auth.js";

/**
 * @description Express Framework Router
 * @param Router
 */
let ContactRouter = express.Router();
ContactRouter.post("/addContact",AuthMiddleware.auth,ContactController.addContact);  
ContactRouter.get("/getContact",AuthMiddleware.auth,ContactController.getContact);  
ContactRouter.post("/updateContact/:id",AuthMiddleware.auth,ContactController.updateContact);  
ContactRouter.delete("/deleteContact/:id",AuthMiddleware.auth,ContactController.deleteContact);  


/**
 * @description Configured router for User Routes
 * @exports ContactRouter
 * @default
 */
export default ContactRouter;
