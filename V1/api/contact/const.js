const ContactConstants = {

  contactFeatched: "Contact featched successfully",
  contactSaved: "Contact saved successfully",
  contactNotFound: "Contact does not exist",
  contactDeleted: "Contact deleted succssfully", 
  fullNameRequired: "Full name is required",
  mobileRequired: "Mobile is required",
  emailRequired: "Email is required",
  validationError :"Validateion error",
  contactId:"contact not found"

};

export { ContactConstants }; 
