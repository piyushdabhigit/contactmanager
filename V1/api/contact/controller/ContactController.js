

import mongoose from "mongoose";
import ContactModel from "../../../../models/ContactModel.js";
import validator from "express-validator";
const { body, header, validationResult } = validator;

//const validator = require("validator");
/**
 * @description API Response Utility functions
 * @param {Function} successResponse - Success Response with message
 * @param successResponseWithData - Success Response with data
 * @param errorResponse - Error Response with message
 * @param errorResponseWithData - Error Response with data
 * @param validationErrorWithData - Validation Error with data
 * @param validationError - Validation Error with message
 * @param unauthorizedResponse - Unauthorized Error response handling
 * @param unprocessable - Unprocessable Error response handling
 */
import {
    successResponse,
    successResponseWithData,
    errorResponse,
    errorResponseWithData,
    notFoundResponse,
    validationErrorWithData,
    validationError,
    unauthorizedResponse,
    unprocessable,
} from "../../utils/apiResponse.js";

/**
 * @description User Constants
 * @param ContactConstants
 */
import { ContactConstants } from "../const.js";
import { middlewaresConstants } from "../../../middlewares/const.js";

const getContact = [

    async (req, res) => {

        try {
 
            let contact = await ContactModel.find({ userId: mongoose.Types.ObjectId(req.users.id) });
            return successResponseWithData(res, ContactConstants.rolefeatched, contact)

        }
        catch (e) {
            return errorResponse(res, e);
        }

    }]
const addContact = [
    // Validate and Sanitize fields.   
    body("fullName")
        .isString()
        .trim()
        .escape()
        .withMessage(ContactConstants.fullNameRequired),
    body("email")
        .isString()
        .trim()
        .escape()
        .withMessage(ContactConstants.emailRequired),
    body("mobile")
        .isString()
        .trim()
        .escape()
        .withMessage(ContactConstants.mobileRequired),
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // Display sanitized values/errors messages.
            return validationErrorWithData(
                res,
                ContactConstants.validationError,
                errors.array()
            );
        }
        else {
            try {
                const { users } = req;
                console.log(users)
                console.log('a', req.users)
                let contactObj = await ContactModel.aggregate([
                    {$match: {
                      $and: [
                        {mobile: req.body.mobile},
                        {userId : mongoose.Types.ObjectId(req.users.id)},
                       ]
                    }
                    }, 
                ])
                if (!contactObj) {

                    let contact = new ContactModel(req.body);
                    contact.userId = mongoose.Types.ObjectId(req.users.id);
                    console.log('a0', contact)
                    await contact.save();

                    return successResponseWithData(res, ContactConstants.contactSaved, contact)
                }
                else {
                    return errorResponse(res, "Mobile is already exist");
                }

            }
            catch (e) {
                return errorResponse(res, e);
            }
        }
    }]
const updateContact = [
   /// param("Id").isMongoId().withMessage(ReelConstants.ContactId),

    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // Display sanitized values/errors messages.
            return validationErrorWithData(
                res,
                ReelConstants.validationError,
                errors.array()
            );
        } 
        else { 
            const { id } = req.params;
          
            const { fullName, email, mobile } = req.body;
      
            try {
      
                let contactObj = await ContactModel.aggregate([
                    {$match: {
                      $and: [
                        {mobile: req.body.mobile},
                        {userId : mongoose.Types.ObjectId(req.users.id)},
                       ]
                    }
                    }, 
                ])
                if (!contactObj) {

                    // let contact = new ContactModel(req.body);
                    // contact.userId = mongoose.Types.ObjectId(req.users.id);
                     
                    await ContactModel.findByIdAndUpdate( mongoose.Types.ObjectId(req.users.id), req.body);

                    return successResponseWithData(res, "Contact Updated Successfully")
                }
                else {
                    return errorResponse(res, "Mobile is already exist");
                }

      
              
              //return successResponse(res, commentConstants.commentUpdated);
            }
            catch (err) {
              return ErrorResponse(res, ContactConstants.err);
            }

            
        }
       

    }]
const deleteContact = [

    async (req, res) => {

        try {

            const deleteRole = await ContactModel.deleteOne({
                _id: mongoose.Types.ObjectId(req.params._id)
            });

            if (deleteRole.deletedCount != 0) {
                return successResponse(res, ContactConstants.contactDeleted);
            }

        }
        catch (e) {
            return errorResponse(res, e);
        }

    }]

export default {
    addContact,
    getContact,
    updateContact,
    deleteContact
};
