/**
 * @description Express Framework module
 * @param express
 */
import express from "express";

/**
 * @description User Controller
 * @param UsersController
 */

import UsersController from "../controller/UsersController.js";

import jwt from "../../../middlewares/jwt.js"
// import upload from '../../../middlewares/upload.js';

/**
 * @description Express Framework Router
 * @param Router
 */
let UsersRouter = express.Router();


UsersRouter.post("/login",UsersController.login); 
UsersRouter.post("/register",UsersController.register);  


/**
 * @description Configured router for User Routes
 * @exports UsersRouter
 * @default
 */
export default UsersRouter;
