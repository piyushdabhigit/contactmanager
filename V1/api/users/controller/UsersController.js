

import mongoose from "mongoose";
import UsersModel from "../../../../models/usersModel.js";
import validator from "express-validator";
import md5 from "md5";
import jwt from "jsonwebtoken";
const { body, header, validationResult } = validator;

//const validator = require("validator");
/**
 * @description API Response Utility functions
 * @param {Function} successResponse - Success Response with message
 * @param successResponseWithData - Success Response with data
 * @param errorResponse - Error Response with message
 * @param errorResponseWithData - Error Response with data
 * @param validationErrorWithData - Validation Error with data
 * @param validationError - Validation Error with message
 * @param unauthorizedResponse - Unauthorized Error response handling
 * @param unprocessable - Unprocessable Error response handling
 */
import {
    successResponse,
    successResponseWithData,
    errorResponse,
    errorResponseWithData,
    notFoundResponse,
    validationErrorWithData,
    validationError,
    unauthorizedResponse,
    unprocessable,
} from "../../utils/apiResponse.js";
 
/**
 * @description User Constants
 * @param UsersConstants
 */
import { UsersConstants } from "../const.js";
import { middlewaresConstants } from "../../../middlewares/const.js";
import crypto from "crypto";


const register = [
    body("email")
        .isString()
        .trim()
        .escape()
        .withMessage(UsersConstants.emailRequired),
    body("password")
        .isString()
        .trim()
        .escape()
        .withMessage(UsersConstants.passwordRequired),
    body("fullName")
        .isString()
        .trim()
        .escape()
        .withMessage(UsersConstants.fullNameRequired),
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          // Display sanitized values/errors messages.
          return validationErrorWithData(
            res,
            UsersConstants.validationError,
            errors.array()
          );
        } 
        else {
    
            try {

                const { email, password, fullName } = req.body; 
            
                var usersObj = await UsersModel.findOne({ email: email })
                if (usersObj) {
                    return errorResponse(res, "User with this email is already exist!");

                }
                else { 
                        //User Registration
                        let users = new UsersModel(req.body); 
                        users.password = md5(password); 
                        await users.save(); 
                        return successResponseWithData(res, UsersConstants.userSavedMsg, users)
                    
                }
            }
            catch (e) {
                console.log(e)
                return errorResponse(res, e);
            }
        }

    }]



const login = [
    // Validate and Sanitize fields.    

    body("email")
        .isString()
        .trim()
        .escape()
        .withMessage(UsersConstants.emailRequired),
    body("password")
        .isString()
        .trim()
        .escape()
        .withMessage(UsersConstants.passwordRequired),
    // Process request after validation and sanitization.
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // Display sanitized values/errors messages.
            return validationErrorWithData(
              res,
              UsersConstants.validationError,
              errors.array()
            );
          } 
          else {
            try {
                const { email, password } = req.body;

                let users = await UsersModel.findOne({ email: email });


                if (users) {

                    if (users.password == md5(password)) {

                        const authToken = jwt.sign({ id: users.id }, process.env.JWT_SECRET);

                        var updateToken = await UsersModel.updateOne({
                            "_id": mongoose.Types.ObjectId(users.id),
                        }, {
                            '$set': {
                                "authToken": authToken
                            }
                        })

                        // //Create JWT payload
                        // const jwtPayload = {
                        //     _id: users._id,
                        // // identity: identity
                        // };
                        // const userResponse = jwtPayload;  
                        // const secret = process.env.JWT_SECRET; 
                        // jwtPayload.token = jwt.sign(jwtPayload, secret);
                         
                        users.authToken = authToken;
                        
                         
                        // users = await UsersModel.findOne({ email: email });
                        return successResponseWithData(res, UsersConstants.userFetchMsg, users);
                        //return res.status(200).json({ message: 'User LoggedIn Successfully',users,token})
                        
                    }
                    else {
                        return errorResponse(res, "Invalid Credentials, Please Try Again.");
                    }

                }
                else {
                    return errorResponse(res, UsersConstants.noUserFoundMsg);
                }

            }
            catch (e) {
                return errorResponse(res, e);
            }
        }
    }]


 


export default {
    login,
    register
};
