const UsersConstants = {
  validationError :"Validateion error",
  countryRequired: "Country is required",
  countryCodeRequired: "Country Code is required",
  fullNameRequired: "Full name is required",
  passwordRequired: "Password is required",
  emailRequired: "Email is required",
  mobileRequired: "Mobile no is required",
  mobileRequired: "Mobile Number or Email is required",
  otpValidationError: "OTP must be specified.",
  errorOccurred: "Error Occured", 
  noUserFoundMsg: "No user found", 
  inSufficientBalance:"You have insufficient balance.",
  validationError: "Validation Error",
  loginSuccessMsg: "Loggedin Successfully",
  invalidLogin:"Invalid credentials. Please try again.", 
  invalidMobile:"Please enter valid mobile number",
  userFetchMsg: "User loggedin successfully",
  emailExist: "Email is already exist.",
  emailNotMatched: "Email does not connected with this mobile number.",
  mobileNotMatched: "Mobile does not connected with this email.",
  userSavedMsg: "User registered successfully",
  mobileExist: "Mobile is already exist.",
  userNotFound:"User not found",
  
  
};

export { UsersConstants }; 
