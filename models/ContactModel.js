import mongoose from "mongoose";
const Schema = mongoose.Schema; 
import validator from "validator";
var ContactSchema = new mongoose.Schema(
  { 
    userId:{type: mongoose.Schema.Types.ObjectId},
    fullName: { type: String }, 
    email: {
      type: String,
      trim: true,
      unique: true,
      lowercase: true,
      dropDups: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Email is not valid.");
        }
      }
    },
    mobile: {
      type: String, unique: true, minlength: 10,
      validate(value) {
        if (!validator.isMobilePhone(value, 'en-IN')) {
          throw new Error("Mobile number is not valid.");
        }
      }
    },
  },
  { timestamps: true },
  { collection: 'contact' });

 
ContactSchema.pre("save", function (next) {
  this.updatedAt = new Date();
  next();
});

const contact = mongoose.models.contact || mongoose.model("contact", ContactSchema);
export default contact;